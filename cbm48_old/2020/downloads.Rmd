---
title: "Ke stažení"
---

![](images/cbm48_banner.png)

\  

[Denik CBM48 (PDF)](images/denik_cbm48.pdf)

[Denik CBM48 větší (PDF)](images/denik_cbm48_large.pdf)

[Mapa CBM48 (JPG)](images/kontrol_cbm48_mapa.jpg)

[Mapa CBM48 bledší (JPG)](images/kontrol_cbm48_mapa2.jpg)
