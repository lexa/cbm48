---
title: "Ke stažení"
---

![](images/cbm48_banner.png)

\
  
[Vizuální pomůcka - přidělěný znak (JPG)](images/prideleny_znak.jpg)

[Vizuální pomůcka - jak na lokátory v přesunu (JPG)](images/jak_na_lokatory.jpg)

[Denik CBM48 papír (PDF)](images/denik_cbm48.pdf)

[Denik CBM48 papír větší řádky (PDF)](images/denik_cbm48_large.pdf)

[Denik CBM48 Excel podle expedice Budvar](images/vzor_ala_budvar.xlsx) - díky jihočechům za sdílení

[Mapa CBM48 (JPG)](images/kontrol_cbm48_mapa.jpg)

[Mapa CBM48 bledší (JPG)](images/kontrol_cbm48_mapa2.jpg)

