---
title: "Vyhodnocení CBM48"
---

![](images/cbm48_banner.png)

\  

 * 29.12.2020 - Bodovací tabulka
 
 ![](images/vysledky_2020.png)
 
 [Článek na cbdx.cz](https://cbdx.cz/clanky/vsechny/mates-brno-na-toku)

 * 9.12.2020 - Výsledky dneškem oficiální (beze změn) 

[Diplomy vítězům - blahopřejeme!](images/diplomy_2020.jpg)

 * 29.11.2020 - předběžné výsledky 
 
 Konečně jsem si našel půl dne klidu pro dokončení vyhodnocení. Protože nemám úplně dotaženy různé kontroly deníků (např. vůči kontrolkám) a taky nechám krátký čas na řešení případných reakcí soutěžících, berte výsledky tak na 98.5%. Mezičasem zveřejním článek o soutěži a jejím průběhu a připravím podrobnou tabulku kde kdo a jak získal body.

Pokud jde o taktiku, o 2.-3.místo se porvali exp.Bažanti a exp.Kameň. Zatím co jedni skombinovali výjezd na dobrou kótu s krátkým zavysíláním z domova, druzí využili letošní vysoký koeficient za pěší výlet a povedlo se jim spojení z V Slovenska na některé expedice na S a Z země. Oběma blahopřeji k úspěchu! První místo nás už asi vícero tušilo, když jsme slyšeli expedici Tetřev vysílat cca 43.5 hodiny ze čtyř různých míst na S ČR. Blahopřeji Tetřevovi! Myslím, že diplomy a ceny půjdou do dobrých rukou a doufám, že další taky potěší pohled do tabulky a vzpomínka na akci na pásmu.

Soutěžní znak | Volačka | Body (předběžně)
---- | --------------- | --- 
C01 | exp.Tetřev | 335.4
S02 | exp.Bažanti | 141.2
S06 | exp.Kameň | 131.6
C03 | Apache Znojmo | 117.6
C05 | DJ Brno | 91.5
S03 | exp.Kojoti | 81.6
C02 | exp.Fox | 80
S05 | exp.Matej | 78.6
C11 | David+Štěpán Stod | 68.7
C13 | Petr Lipník | 60.6
S04 | exp.Perún | 60.5
S01 | Marián Michalková | 16.5
C09 | Salamandr V.Bystřice | 16.5
C07 | exp.Pekla | 13
C10 | Proton Praha | 9
C06 | Petr Medlánky | 3
C12 | Josef Sopotnice | 1
X02 | Kamilo Rakousko | 1
C04 | Jirka Jenštejn | 1
C08 | Pavel Opava | ?
X01 | Skipper Kitsee | Nestartoval


 * 11.11.2020 - poděkování za startovné
 
Vyhodnocení se už blíží. Mezičasem, dovolte abych tímto způsobem poděkoval účastníkům, kteří přispěli 
na organizaci soutěže poukázaním dobrovolného startovného. Výslední částka mě docela (příjemně) překvapila 
a znamená, že letos budem moci být štědřejší k výhercům a na rozdíl od loňska nebudu muset hradit ceny výhercům (mám samozřejmě i jiné výdaje). Celkově se vybralo Kč 1650,- které budou dle 
propozic rozdělěny na ceny pro soutěžící na prvních tří místech, ceny plánuji jako poukázky na online nákup v hodnotě Kč 800, 500 a 300, s tím, že 50 ponechám do příštího ročníku.

A nakonec poděkování za uhrazení startovného v pořadí od nejvyšší částky:

		+ exp.Matej
		+ exp.Tetřev
		+ Kamillo Austria
		+ exp.Kameň
		+ DJ Brno
   
Dík za podporu i touto formou!

 * 2.11.2020 - ukončen sběr deníků

Na mém disku přistáli takřka všechny deníky, budu se teď snažit co nejrychleji udělat vyhodnocení. Zatím se podívejte na fotky, co mi z CB Maratonu poslali někteří účastníci. Pokud máte zajímavé foto taky, sem s ním! 

 * 11.10.2020 - zahájen sběr deníků

CBM48 2020 máme za náma. Děkuji tímto všem kontrolkám (exp.Echo, Fugas a Jitřenka Brno), že svou aktivitou pomáhali udržovat soutěž při životě a motivovali soutěžící k výkonům nebo jim radili jak na to. Děkuji těm, co poslali startovné, bude použito na ceny pro vítěze. Děkuji soutěžícím za projevený zájem a všem na pásmu za spojení se soutěžícími a další podporu.

Budu vděčný za postřehy a připomínky. Pokud mi někdo pošle nějaké fotky či jiný materiál, můžu zde zveřejnit.

Přijaté deníky budou označeny v seznamu přihlášených červenou poradovou číslicí.

# Fotogalerie CBM48 2020

Fotky, které nám zaslal Venda Brdy z expedic Tetřev.

Ještědka:
![](images/cbm48_tetrev1.jpg)

Kozákov:
![](images/cbm48_tetrev2.jpg)

QSL lístek svého druhu, který vysílala kontrolka Mates Brno v sobotu a neděli 22:30 v kódování SSTV PD120. V sobotu se nám takhle povedlo navázat spojení s Milanem Tábor! Děujeme Milane za promptní reakci, i když jsem čekali odpovědě až v jedenáct, povedlo se nám nakonec na nějakém mobilu zapnout nahrávání a druhou polovinu zachytit. Naštěstí Milan měl víc rozumu jak my a volačku umístil do spodní části obrázku.

![](images/cbm48_sstv.jpg)

A ještě něco fotek kolem kontrolky, která ale na kótě už neměla čas pózovat.

Portejbl začíná vlastně v Praze.
![](images/cbm48_tok1.jpg)

Nový recept Matese Brno, muchomůrka na sdílenkách.
![](images/cbm48_tok2.jpg)

Tok je krásný.
![](images/cbm48_tok3.jpg)

Proton Praha nenechával nic na náhodu a bumerang na balkoně paneláku nahradil...

...něčím větším.
![](images/cbm48_proton.jpg)

Expedice Pekla se může taky pochlubit nádherným QTH! Vysílali jen krátce, ale doletělo jim to na Vysočinu i na Slovensko.

![](images/cbm48_pekla1.jpg)

![](images/cbm48_pekla2.jpg)

Kojoti v akci na Jankovom vŕšku, Machnáčni a Velké Javořině
![](images/kojoti1.jpg)

![](images/kojoti2.jpg)

![](images/kojoti3.jpg)

